<!DOCTYPE html>
<html>
<body>
<?php 

//include setup file, tests path of DB json file etc
require 'inc/setup.inc.php';

//include base class
require 'inc/base.inc.php';

//include db class
require 'inc/db.inc.php';

//include all class files
foreach (glob("classes/*.php") as $filename) {
    include $filename;
}


$cases =  new cases;

if (isset($_GET['mode']) && $_GET['mode']) {
	$cases->mode = $_GET['mode'];
} else {
	$cases->mode = "list_cases";
}

//Show any error or success messages at top of page
if (isset($_POST['action']) && $_POST['action']) {
	$result = $cases->process($_POST['action']);
	if (isset($result['error']) && $result['error']) {
		echo "<p class='error'>".htmlspecialchars($result['error'])."</p>";
	}
	if (isset($result['success']) && $result['success']) {
		echo "<p class='success'>".htmlspecialchars($result['success'])."</p>";
	}
}

$html = $cases->output($cases->mode);

echo $html;

?>
</body>
</html>