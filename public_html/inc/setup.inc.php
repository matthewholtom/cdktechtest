<?php
//JSON Database filename:
$DBFilename = 'cases.json';

//JSON file directory relitive path from index.php he path should end in /
//This file should be outside of the public html directory do that it is now accessible via the browser.
$DBDirectory =  '../';

$path = $DBDirectory . $DBFilename;

if (file_exists($path)) {
	define('DB_PATH', $path);
} else { 
	exit("Unable to access Database File.");
}
