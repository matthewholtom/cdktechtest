<?php

interface base {
	
	//method should retiurn html for given mode
	public function output($mode);
	
	//method should process data e.g. create edit delete for given mode 
	public function process($mode);
	
}