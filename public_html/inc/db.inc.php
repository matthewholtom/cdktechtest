<?php

abstract class db {
	
	public function castToType($type, &$value) {
		switch ($type) {
				case "int":
					$value = (int) $value;
					break;
				case "string":
					$value = (string) $value;
					break;
				default:
					$value = false;
					break;
		}
		return $value;
	}
	
	public function loadDatabase() {
		$rawFile = file_get_contents(DB_PATH) or die("Error , unable to read DB file.");
		$cases = json_decode($rawFile, true);
		
		return $cases;
	}
	
	public function saveDatabase($dbArray) {
		$contents = json_encode($dbArray);
		//a lock should be implemented from when DB is updated.
		file_put_contents(DB_PATH, $contents) or die("Error , unable to write to DB file.");
		
		return true;
	}
	
}