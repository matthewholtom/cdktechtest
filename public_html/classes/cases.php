<?php 

class cases implements base {
	
	public $mode ="";
	
	protected $addCommentSchema = [
		"CommentBody" => ["required" => true, "type" => "string"],
		"CreatedBy" => ["required" => true, "type" => "string"]
	];
	
	public function output($mode) {
		$vaildModes = ['add_comment', 'edit_comment', 'view_case', 'list_cases'];
		
		if (in_array($mode, $vaildModes)) {
			$mode = 'output_' . $mode;
			$html = $this->$mode();
		} else { 
			//Show default_mode
			$html = $this->output_list_cases();
		}
		
		return $html;
	}
	
	private function output_add_comment() {
		if (!isset($_REQUEST['CaseNumber']) || !$_REQUEST['CaseNumber'] ) {
			$html = '<p class="error">Error, no CaseNumber given.</p>'; 
			return $html;
		}
		if (!$case = self::getCase($_REQUEST['CaseNumber'])) {
			$html = '<p class="error">No case found with CaseNumber ' . htmlspecialchars($_REQUEST['CaseNumber']) . '</p>'; 
			return $html;
		}
		
		$html = '
			<p><a href="?mode=view_case&CaseNumber=' .urlencode($case['CaseNumber']). '">Back to Case</a></p>
			<h1>Comments for for Case ' .  htmlspecialchars($case['CaseNumber']) . '</h1>';
		
		if (isset($case['Comments']) && $case['Comments']) {
			//build list of previous comments
			$html .= '<table>
							<tr><th>ID</th><th>Date</th><th>Created by</th><th>Comment</th></tr>';
			foreach ($case['Comments'] as $comment) {
				$html .= '<tr>
								<td>' . htmlspecialchars($comment['Id']) . '</td>
								<td>' . date("F j, Y, g:i a", strtotime($comment['CommentDate'])) . '</td>
								<td>' . htmlspecialchars($comment['CreatedBy']) . '</td>
								<td>' . htmlspecialchars($comment['CommentBody']) . '</td>
							</tr>';
			}
			
			$html .= '</table>';
			
		} else {
			$html .= "<p>No previous comments.</p>";
		}
		
		//build html for add comment form
		$html .= '
			<h2>Add a new comment:</h2>
			<form action="index.php?" method="post">
				<input type="hidden" name="action" value="add_comment">
				<input type="hidden" name="CaseNumber" value="' .  htmlspecialchars($case['CaseNumber']) . '">
				<div class="input_wrap">
					<p>Your Name:</p>
					<input type="text" name="CreatedBy" required>
				</div>
				<div class="input_wrap">
					<p>Comment:</p>
					<textarea rows="10" cols="50" name="CommentBody" required></textarea>
				</div>
				<input type="submit" value="Add comment"> 
			</form>
		';
		return $html;
	}
	
	private function output_view_case(){
	
		if (!isset($_REQUEST['CaseNumber']) || !$_REQUEST['CaseNumber'] ) {
			$html = '<p class="error">Error, no CaseNumber given.</p>'; 
			return $html;
		}
		if (!$case = self::getCase($_REQUEST['CaseNumber'])) {
			$html = '<p class="error">No case found with CaseNumber ' . htmlspecialchars($_REQUEST['CaseNumber']) . '</p>'; 
			return $html;
		}
			
		$html = '
			<p><a href="?mode=list_cases">Back to Cases List</a></p>
			<h1>Viewing Case ' . htmlspecialchars($case['CaseNumber']) . ':</h1>
			<table>
				<tr>
					<td>CaseNumber:</td>
					<td>'.htmlspecialchars($case['CaseNumber']).'</td>
				</tr>
				<tr>
					<td>Subject:</td>
					<td>'.htmlspecialchars($case['Subject']).'</td>
					</tr>
				<tr>
					<td>Description:</td>
					<td>'.htmlspecialchars($case['Description']).'</td>
				</tr>
				<tr>
					<td>Status:</td>
					<td>'.htmlspecialchars($case['Status']).'</td>
				</tr>
				<tr>
					<td>CreatedDate:</td>
					<td>'.date("Y-m-d g:ia", strtotime($case['CreatedDate'])).'</td>
				</tr>
				<tr>
					<td>LastModifiedDate:</td>
					<td>'.date("Y-m-d g:ia", strtotime($case['LastModifiedDate'])).'</td>
				</tr>
				<tr>
					<td>Owner:</td>
					<td>'.htmlspecialchars($case['Owner']['FirstName'].' '.$case['Owner']['LastName']). '</td>
					</tr>
				<tr>
					<td>Contact:</td>
					<td>'.htmlspecialchars($case['Contact']['FirstName'].' '.$case['Contact']['LastName']). '<br>'.htmlspecialchars($case['Contact']['Email']) . '<br>'.htmlspecialchars($case['Contact']['Phone']) .'</td>
				</tr>
				<tr>
					<td>Priority:</td>
					<td>'.htmlspecialchars($case['Priority']).'</td>
				</tr>
				<tr>
					<td>Product:</td>
					<td>'.htmlspecialchars($case['Product']).'</td>
				</tr>
				<tr>
					<td>ProductGroup:</td>
					<td>'.htmlspecialchars($case['ProductGroup']).'</td>
				</tr>
				<tr>
					<td>Impact:</td>
					<td>'.htmlspecialchars($case['Priority']).'</td>
				</tr>
				<tr>
					<td>Severity:</td>
					<td>'.htmlspecialchars($case['Product']).'</td>
				</tr>
				<tr>
					<td>VIP:</td>
					<td>'.htmlspecialchars($case['ProductGroup']).'</td>
				</tr>
			</table>';
		if (isset($case['Comments']) && $case['Comments']) {
			//build list of previous comments
			$html .= '<h2>Comments</h2>
						<table>
							<tr><th>ID</th><th>Date</th><th>Created by</th><th>Comment</th></tr>';
			foreach ($case['Comments'] as $comment) {
				$html .= '<tr>
								<td>' . htmlspecialchars($comment['Id']) . '</td>
								<td>' . date("Y-m-d g:ia", strtotime($comment['CommentDate'])) . '</td>
								<td>' . htmlspecialchars($comment['CreatedBy']) . '</td>
								<td>' . htmlspecialchars($comment['CommentBody']) . '</td>
							</tr>';
			}
			$html .= '</table>';
		} else {
			$html .= '<p>No comments.</p>';
		}
		$html .= '<p><a href="?mode=add_comment&CaseNumber=' .urlencode($case['CaseNumber']). '">Add comment</a></p>';
		return $html;
	}
	
	private function output_list_cases() {
		$cases = self::getCases();
		$html = '
			<h1>Cases</h1>';
			
		$html .= '<table>
						<tr><th>CaseNumber</th><th>Subject</th><th>Priority</th><th>Created Date</th><th>Last Modified</th><th></th></tr>';
		foreach ($cases as $case) {
			$html .= '<tr>
							<td>' . htmlspecialchars($case['CaseNumber']) . '</td>
							<td>' . htmlspecialchars($case['Subject']) . '</td>
							<td>' . htmlspecialchars($case['Priority']) . '</td>
							<td>' . date("Y-m-d g:ia", strtotime($case['CreatedDate'])) . '</td>
							<td>' . date("Y-m-d g:ia", strtotime($case['LastModifiedDate'])) . '</td>
							<td><a href="?mode=view_case&CaseNumber=' .urlencode($case['CaseNumber']). '">View</a></td>
						</tr>';
		}
		return $html;
	}
	
	public function process($action) {
		switch ($action) {
			case "add_comment":
				if (isset($_POST['CaseNumber']) && $_POST['CaseNumber']) {
					$comment = [];
					
					//add post data to comment array and case correct var type
					foreach ($this->addCommentSchema as $key => $field) {
						if (isset($_POST[$key]) && $_POST[$key]) {
							$comment[$key] =  db::castToType($field['type'], $_POST[$key]);
						}
					}
					
					//Return to add comment mode
					$this->mode = "add_comment";
					
					if (self::addComment($_POST['CaseNumber'], $comment)) {
						return ['success' => "Your comment was successfully added."];
					} else {
						return ['error' => "Error, sorry your comment could not be added."];
					}
				}
				break;
			case "edit_comment":
				//code for editing comment goes here
				break;
			case "delete_comment":
				//code for deleting comment goes here
				break;
		}
	}
	
	public static function getCase($CaseNumber) {
		$cases = db::loadDatabase();
		
		foreach ($cases as $case) {
			if ($case['CaseNumber'] == $CaseNumber) {
				return $case;
			}
		}
		
		return false;
	}
	
	public static function getCases() {
		$cases = db::loadDatabase();
		return $cases;
	}
	
	private function addComment($CaseNumber, $comment) {
		foreach ($this->addCommentSchema as $key => $field) {
			if ($field['required']) {
				if (!isset($comment[$key]) || !$comment[$key]) {
					//we should show an error message if a required field is not included.
					
					return false;
				}
			}
		}
		
		$cases = db::loadDatabase();
		
		foreach ($cases as &$case) {
			if ($case['CaseNumber'] == $CaseNumber) {
				if (!isset($case['Comments'])) {
					$case['Comments'] = [];
				}
				$numComments = count($case['Comments']);
				$numComments++;
				
				$comment['Id'] = "C-".$numComments;
				
				$datetime = date('Y-m-d') . "T" . date('H:i:s.v')."Z"; //date('c') 	ISO 8601 is similar but not exact
				
				$comment['CommentDate'] = $datetime;
				
				//update case modified time
				$case['LastModifiedDate'] = $datetime;
				
				array_unshift($case['Comments'], $comment);
				
				db::saveDatabase($cases);
				
				return $comment['Id'];
			}
		}
		
		return false;
	}
	
}