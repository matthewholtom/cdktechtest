Cases system with functionality includes:

1. List cases
2. View a cases including it's comments
3. Add a case comment

This system is written in PHP and requires PHP 7.

The system was tested on a Ubuntu 14 server running Apache version 2.4.7 and PHP 7.

I choose PHP as it's the language I am most experienced in.

There are 3 different modes/pages implemented, instead of using one php file per page I decided to use a single index file and created a class for the Cases functionality.
I felt that this would be more maintainable and scaleable in the future.

I kept security in mind whilst developing this solution however I assumed this system would already behind a login system.

Please enter the JSON DB filename and relitive directory path from the public_html directory in the setup.inc.php file. (You shouldn't have to do this if you leave the files in the same stucture as found here)

Please make sure that the JSON DB file is writable by the webserver user e.g. www-data

A live test can be found here: http://dev-mh.tribiq.com/cdktechtest/public_html/

